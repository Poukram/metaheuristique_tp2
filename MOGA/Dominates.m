function res = Dominates(i1, i2, objectives_nb)
%DOMINATION Determines if i1 dominates i2
%   return 1 if i1 dominates i2, 0 otherwise 
nb_inf_or_eq = 0;
nb_strictly_inf = 0;
res = 0;

for n = 1:objectives_nb
    if (i1(n) <= i2(n))
        nb_inf_or_eq = nb_inf_or_eq + 1;
    end
    if (i1(n) < i2(n))
        nb_strictly_inf = nb_strictly_inf + 1;
    end
end

if (nb_inf_or_eq == objectives_nb && nb_strictly_inf > 0)
    res = 1;
end
end

