function output = MOGACaller(param)
%MOGACALLER Summary of this function goes here
%   Detailed explanation goes here
switch param.algo_name
    case 'NSGA2'
        output = NSGA2(param);
    case 'SPEA2'
        output = SPEA2(param);
    otherwise
        warning('This is not a valid MOGA, try a different name')
end
end

