function Pop = NormalMutation(Pop, popsize, pm, pb)
%NORMALMU Summary of this function goes here
%   Detailed explanation goes here
sigma = 0.4;

for n = 1:popsize
    for i = 1:pb.vars_nb
        tmp = rand();
        if (tmp <= pm)
            Pop(n).vars(i) = Pop(n).vars(i) + sigma * normrnd(0,1);
        end
    end
end
end

