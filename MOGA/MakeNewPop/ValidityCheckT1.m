function Pop = ValidityCheckT1(Pop, popsize, pb)
%VALIDITYCHECKT1 Summary of this function goes here
%   Detailed explanation goes here
for n = 1:popsize
    for i = 1:pb.vars_nb
        tmp = Pop(n).vars(i);
        range = pb.vars_range(i,:);
        
        if (tmp < range(1))
            Pop(n).vars(i) = range(1);
        elseif (tmp > range(2))
            Pop(n).vars(i) = range(2);
        end
    end
end

