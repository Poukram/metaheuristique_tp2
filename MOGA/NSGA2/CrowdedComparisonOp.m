function res = CrowdedComparisonOp(src_array, i_idx, j_idx)
%CROWDEDCOMPARISONOP Operates NSGA2 comparison between i and j
%   i and j are the indexes of the elements to compare in the source array
%   Uses return values compatible with the quicksort function used
%   1 -> i <n j / -1 -> j <n i / 0 otherwise   
res = 0;
i = src_array(i_idx);
j = src_array(j_idx);

if (i.rank < j.rank || ( i.rank == j.rank && i.crowd_dist > j.crowd_dist))
    res = 1;
elseif (j.rank < i.rank || ( j.rank == i.rank && j.crowd_dist > i.crowd_dist))
    res = -1;
end
end

