function Pop = CrowdingDistanceCompute(I, pb)
%CROWDINGDISTANCECOMPUTE Summary of this function goes here
%   Detailed explanation goes here

l = size(I,2);

for i = I
    i.crowd_dist = 0;
end

tmp = vertcat(I.objs_vals);
for m = 1:pb.objectives_nb
    [~, idx] = sort(tmp(:,m));
    I = I(idx);
    f_min = I(1).objs_vals(m);
    f_max = I(end).objs_vals(m);
    
    I(1).crowd_dist = inf;
    I(end).crowd_dist = inf;
    for i = 2:l-1
        I(i).crowd_dist = I(i).crowd_dist +...
            (I(i+1).objs_vals(m) + I(i-1).objs_vals(m))/(f_max - f_min);
    end
end
end

