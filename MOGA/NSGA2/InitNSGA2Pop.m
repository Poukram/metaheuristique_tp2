function Pop = InitNSGA2Pop(popsize, pb)
Pop(popsize) = NSGA2_Ind(pb.vars_nb, pb.objectives_nb);

for n = 1:popsize
    Pop(n) = NSGA2_Ind(pb.vars_nb, pb.objectives_nb);
    for k = 1:pb.vars_nb
        Pop(n).vars(k) = GenericProblem.GenRndVarWithRange(pb.vars_range(k,:));
    end
end
end

