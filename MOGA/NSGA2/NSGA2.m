function pareto_front = NSGA2(param)
    % Initialization phase
    pareto_front(param.popsize) = NSGA2_Ind;
    P = InitNSGA2Pop(param.popsize, param.pb);
    EvalPopOfSizeWithProblem(P, param.popsize, param.pb);
    Fronts = FastNonDominatedSort(P, param.pb);
    Q = MakeNewPop(P, param.popsize, param.pc, param.pm, param.pb, param.algo_name);
    
    % Evolutionnary phase
    for g = 1:param.g_max
        R = [P, Q];
        Fronts = FastNonDominatedSort(R, param.pb); 
        i = 1;
        P1 = [];
        while (numel(P1) + numel(Fronts{i}) < param.popsize)
            CrowdingDistanceCompute(Fronts{i}, param.pb);
            P1 = [P1, Fronts{i}];
            i = i + 1;
            if (numel(Fronts) < i)
                warning("On fait quoi?")
            end
        end
        Fronts{i} = Fronts{i}(quicksort(Fronts{i}, @CrowdedComparisonOp));
        P1 = [P1, Fronts{i}(1:(param.popsize - numel(P1)))];
        Q1 = MakeNewPop(P1, param.popsize,param.pc, param.pm, param.pb,  param.algo_name);
        
        pareto_front = copy(P);
        P = P1;
        Q = Q1;
    end
end

