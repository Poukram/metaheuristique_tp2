function Children = NSGA2LaplaceCrossover(Pop, popsize, pc, pb)
%LAPLACECO Summary of this function goes here
%   Detailed explanation goes here
Children = NSGA2_Ind.GetNewArray(popsize);
perm = randperm(popsize);
n = 0;
a = 0;
b = 0.3;

while(n < popsize)
    p1 = Pop(perm(n + 1));
    p2 = Pop(perm(n + 2));
    
    tmp = rand();
    
    if(tmp <= pc)
        for i = 1:pb.vars_nb
            alpha = rand();
            if (alpha <= 0.5)
                beta = a - b * log(alpha);
            else
                beta = a + b * log(alpha);
            end
            
            Children(n + 1).vars(i) = p1.vars(i) + beta * abs(p1.vars(i) - p2.vars(i));    
            Children(n + 2).vars(i) = p2.vars(i) + beta * abs(p1.vars(i) - p2.vars(i));
        end
    else
        Children(n + 1).vars = p1.vars;
        Children(n + 2).vars = p2.vars;
    end
    
    n = n +2;
end
end

