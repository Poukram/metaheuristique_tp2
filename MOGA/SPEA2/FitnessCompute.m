function FitnessCompute(Pop, popsize, pb)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

% Comuting strength
for i = Pop
    for j = Pop
        if (Dominates(i.objs_vals, j.objs_vals, pb.objectives_nb) == 1)
            i.strength = i.strength + 1;
        end
    end
end

% Computing raw fitness
for i = Pop
    res = 0;
    for j = Pop
        if (Dominates(j.objs_vals, i.objs_vals, pb.objectives_nb) == 1)
            res = res + j.strength;
        end
    end
    i.raw_fit = res;
end

% Computing distances
for i = Pop
    i.dist_arr = zeros(popsize);
    k = 0;
    for j = Pop
        k = k + 1;
        i.dist_arr(k) = sqrt(sum((i.objs_vals - j.objs_vals).^2));
    end
    i.dist_arr = sort(i.dist_arr);
end

% Comuting density and fitness
k = round(sqrt(popsize));
for i = Pop
    i.density = 1 / (i.dist_arr(k) + 2);
    i.fitness = i.raw_fit + i.density;
end
end
