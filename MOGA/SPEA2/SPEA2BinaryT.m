function MatingPool = SPEA2BinaryT(Pop, popsize)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
MatingPool(popsize) = SPEA2_Ind(0,0);
selsize = numel(Pop);

for i = 1:popsize
    i1 = Pop(randi(selsize));
    i2 = Pop(randi(selsize));
    if (i1.fitness <= i2.fitness)
        MatingPool(i) = i1;
    else
        MatingPool(i) = i2;
    end
end
end

