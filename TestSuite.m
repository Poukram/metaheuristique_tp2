function TestSuite(base_param, pb_names)
%TESTSUITE Summary of this function goes here
%   Detailed explanation goes here
pb_nb = numel(pb_names);
nb_tests_to_run = 4;
total_result = cell(1, nb_tests_to_run);
excell_file = "test_data.xlsx";
legend_column = {'Problem name'; 'Best Algorithm (mean)'; 'Best Algorithm (std)';...
    'NSGA2 time (mean)'; 'NSGA2 time (std)'; 'SPEA2 time (mean)'; 'SPEA2 time (std)'};

parfor k = 1:nb_tests_to_run
    pareto_front = repmat(Individual, 2, pb_nb, base_param.popsize);
    completion_time = zeros(2, pb_nb);
    who_dom = zeros(1,pb_nb); % 1 if NSGA dominates 0 otherwise
    res_cell = cell(3,pb_nb);
    
    NSGA2_param = base_param;
    NSGA2_param.algo_name = 'NSGA2';
    
    SPEA2_param = base_param;
    SPEA2_param.archsize = base_param.popsize;
    SPEA2_param.popsize = base_param.popsize * 2;
    SPEA2_param.g_max = base_param.g_max / 2;
    SPEA2_param.algo_name = 'SPEA2';
    
    for i = 1:pb_nb
        disp('Working on ' + pb_names(i))
        pb = GenericProblem(pb_names(i));
        
        param_n = NSGA2_param;
        param_n.pb = pb;
        param_s = SPEA2_param;
        param_s.pb = pb;
        
        start_t_nsga2 = tic;
        nsga2_pf = NSGA2(param_n);
        completion_time(1, i) = toc(start_t_nsga2);
        
        start_t_spea2 = tic;
        spea2_pf = SPEA2(param_s);
        completion_time(2, i) = toc(start_t_spea2);
        
        pareto_front(1, i, :) = nsga2_pf;
        pareto_front(2, i, :) = spea2_pf;
        
        who_dom(i) = Dominates(pareto_front(1,i,:), pareto_front(2,i,:), pb.objectives_nb);
        
        pf = vertcat(nsga2_pf.objs_vals);
        title('Pareto front for ' + pb_names(i) + ' with NSGA2');
        scatter(pf(:,1), pf(:,2));
        saveas(gcf,'pf_'+pb_names(i)+'_nsga2','png');
        
        pf = vertcat(spea2_pf.objs_vals);
        title('Pareto front for ' + pb_names(i) + ' with SPEA2');
        scatter(pf(:,1), pf(:,2));
        saveas(gcf,'pf_'+pb_names(i)+'_spea2','png');
    end
    res_cell(1,:) = num2cell(who_dom(:));
    res_cell(2:3,:) = num2cell(completion_time(:,:));
    total_result(k) = {res_cell};
end
def_res = cell(7,pb_nb+1);
def_res(1,2:end) = cellstr(pb_names);
def_res(:,1) = legend_column;

for i = 1:pb_nb
    for j = 1:3
        tmp = zeros(1,nb_tests_to_run);
        for k = 1:nb_tests_to_run
            tmp(k) = total_result{k}{j,i};
        end
        def_res(((j-1)*2)+2, i+1) = {mean(tmp)};
        def_res(((j-1)*2)+3, i+1) = {std(tmp)};
    end
end
disp('Test suite over')
end

