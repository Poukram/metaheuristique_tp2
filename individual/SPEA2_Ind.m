classdef SPEA2_Ind < Individual
    %SPEA2_IND Summary of this class goes here
    %   Detailed explanation goes here
    
    properties       
        strength
        raw_fit
        density
        fitness
        dist_arr
    end
    
    methods
        function obj = SPEA2_Ind(vars, objs_vals)
            %SPEA2_IND Construct an instance of this class
            %   Detailed explanation goes here
            if (nargin == 0)
                vars = 0;
                objs_vals = 0;
            end
            obj = obj@Individual(vars, objs_vals);
            obj.strength = 0;
            obj.raw_fit = 0;
            obj.density = 0;
            obj.fitness = 0;
            obj.dist_arr = [];
        end
    end
    methods(Access = protected)
        % Deep copy mechanism
        function cp = copyElement(obj)
            cp = SPEA2_Ind;
            cp.strength = obj.strength;
            cp.raw_fit = obj.raw_fit;
            cp.density = obj.density;
            cp.fitness = obj.fitness;
            cp.dist_arr = [];
            % Inherited values
            cp.vars = obj.vars;
            cp.objs_vals = obj.objs_vals;
        end
    end
    methods(Static)
        function obj_arr = GetNewArray(dim)
            obj_arr(dim) = SPEA2_Ind;
            for i = 1:dim
                obj_arr(i) = SPEA2_Ind;
            end
        end
    end
end

