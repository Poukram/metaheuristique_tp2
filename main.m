%------------Configuring the path and initializing -------------------
clear variables
close all
clc
addpath(genpath(pwd))

pb_names = ["FON", "KUR", "POL", "SCH", "ZDT1", "ZDT2", "ZDT3", "ZDT4", "ZDT6"];
%---------------------------------------------------------------------

param.popsize = 10;
param.archsize = 10;
param.g_max = 10; % The maximum number of generations
param.pc = 0.75; % The probability of a crossover to happen
param.pm = 0.0125; % The probability of a mutation to happen

%------------Call to the genetic algorithm-----------
TestSuite(param, pb_names);