classdef GenericProblem
    %GenericProblem Stores information about the problems to solve
    
    properties
        name
        type % 0 for minimization 1 for maximization
        vars_range
        vars_nb
        objectives_nb
        objectives % A cell array of function handles
    end
    
    methods
        function obj = GenericProblem(name)
            obj.name = name;
            obj.type = 0; % In this TP all problems are minimizations
            obj.objectives_nb = 2; % In this TP all problems have 2 objectives
            
            switch name
                case "FON"
                    obj.vars_nb = 3;
                    obj.vars_range = repmat([-4, 4],obj.vars_nb,1);
                    obj.objectives = @fon;
                case "KUR"
                    obj.vars_nb = 3;
                    obj.vars_range = repmat([-5, 5],obj.vars_nb,1);
                    obj.objectives = @kur;
                case "POL"
                    obj.vars_nb = 2;
                    obj.vars_range = repmat([-pi, pi],obj.vars_nb,1);
                    obj.objectives = @pol;
                case "SCH"
                    obj.vars_nb = 1;
                    obj.vars_range = repmat([-10^3, 10^3], obj.vars_nb, 1);
                    obj.objectives = @sch;
                case "ZDT1"
                    obj.vars_nb = 30;
                    obj.vars_range = repmat([0,1], obj.vars_nb, 1);
                    obj.objectives = @zdt1;
                case "ZDT2"
                    obj.vars_nb = 30;
                    obj.vars_range = repmat([0,1], obj.vars_nb, 1);
                    obj.objectives = @zdt2;
                case "ZDT3"
                    obj.vars_nb = 30;
                    obj.vars_range = repmat([0,1], obj.vars_nb, 1);
                    obj.objectives = @zdt3;
                case "ZDT4"
                    obj.vars_nb = 10;
                    obj.vars_range = [[0,1];repmat([-5,5], obj.vars_nb-1, 1)];
                    obj.objectives = @zdt4;
                case "ZDT6"
                    obj.vars_nb = 10;
                    obj.vars_range = repmat([0,1], obj.vars_nb, 1);
                    obj.objectives = @zdt6;
                otherwise
                    warning("Invalid problem name")
            end
        end
        % used to check if functions are correctly defined
        function plot(obj)
            options = optimoptions('gamultiobj','PopulationSize',60,...
                'ParetoFraction',0.7,'PlotFcn',@gaplotpareto);
            gamultiobj(obj.objectives,obj.vars_nb,...
                [],[],[],[],obj.vars_range(:,1),obj.vars_range(:,2),options);
        end
    end
    methods(Static)
        function rnd_var = GenRndVarWithRange(range)
            rnd_var = rand() * abs(range(2) - range(1)) + range(1);
        end
    end
end

