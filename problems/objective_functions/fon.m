function f = fon(x)
%FON Returns FON's two objective functions
%  functions returned as an array of function handles

f(1) = 1 - exp(-sum((x - 1/sqrt(3)).^2));
f(2) = 1 - exp(-sum((x + 1/sqrt(3)).^2)); 
end