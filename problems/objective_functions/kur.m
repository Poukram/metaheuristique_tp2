function f = kur(x)
%KUR Returns KUR's two objective functions
%   functions returned as an array of function handles
    function res = f1_sum(t)
        res = 0;
        for n = 1:2
            res = res -10*exp(-0.2 *sqrt(t(n)^2 + t(n+1)^2));
        end
    end

f(1) = f1_sum(x);
f(2) = sum(abs(x).^0.8 + 5*sin(x.^3));
end
