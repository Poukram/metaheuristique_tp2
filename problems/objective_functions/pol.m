function f = pol(x)
%POL Returns POL's two objective functions
%   functions returned as an array of function handles
A1 = sin(1)/2 - 2*cos(1) + sin(2) - 3*cos(2)/2;
A2 = 3*sin(1)/2 - cos(1) + 2*sin(2) - cos(2)/2;

B1 = sin(x(1))/2 - 2*cos(x(1)) + sin(x(2)) - 3*cos(x(2))/2;
B2 = 3*sin(x(1)) - cos(x(1)) + 2*sin(x(2)) - cos(x(2))/2;

f(1) = 1 + (A1 - B1)^2 + (A2 - B2)^2;
f(2) = (x(1) + 3)^2 + (x(2) + 1)^2;

end

