function f = sch(x)
%SCH Returns SCH's two objective functions
%   functions returned as an array of function handles
f(1) = x^2;
f(2) = (x-2)^2;
end

