function f = zdt1(x)
%ZDT1 Returns ZDT1's two objective functions
%   functions returned as an array of function handles
    function res = g(t)
        res = 1 + 9 * sum(t(2:end))/29; %(n - 1) = 29 because in ZDT1 n = 30
    end

f(1) = x(1);
f(2) = g(x)*(1-sqrt(x(1)/g(x)));
end

