function f = zdt2(x)
%ZDT2 Returns ZDT2's two objective functions
%   functions returned as an array of function handles
    function res = g(t)
        res = 1 + (9/29)*sum(t(2:end)); %(n - 1) = 29 because in ZDT1 n = 30
    end

f(1) = x(1);
f(2) = g(x)*(1-(x(1)/g(x))^2);
end

