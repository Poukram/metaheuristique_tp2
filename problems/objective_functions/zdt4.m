function f = zdt4(x)
%ZDT4 Returns ZDT4's two objective functions
%   functions returned as an array of function handles
    function res = g(t)
        t = t(2:end);
        res = 91 + sum(t.^2 - 10 * cos(4*pi*t)); %(n - 1) = 29 because in ZDT1 n = 30
    end

f(1) = x(1);
f(2) = g(x)*(1-sqrt(x(1)/g(x)));
end

