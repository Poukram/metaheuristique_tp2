%------------ BEGIN Configuring the path and initializing -------------------
clear variables
close all
clc
addpath(genpath(pwd))
%------------ END Configuring the path and initializing -------------------
param.pb = GenericProblem("ZDT3");
param.popsize = 20;
param.archsize = 10;
param.g_max = 40; % The maximum number of generations
param.pc = 0.75; % The probability of a crossover to happen
param.pm = 0.0125; % Th probability of a mutation to happen
param.algo_name = 'NSGA2';

res = MOGACaller(param);
rf = res(param.g_max,:);
rfov = vertcat(rf.objs_vals);
scatter(rfov(:,1), rfov(:,2))
param.pb.plot()